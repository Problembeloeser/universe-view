package problembeloeser;

import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;

public class MapController {

    @FXML
    private AnchorPane pane;

    @FXML
    public void initialize() {
        pane.getChildren().add(PlanetTerrainGenerator.generateTerrain(800, 400, 1));
    }

}
