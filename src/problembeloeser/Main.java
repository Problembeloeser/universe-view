package problembeloeser;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Camera;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import javafx.scene.transform.Transform;
import javafx.scene.transform.Translate;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Main extends Application {

    private static boolean doGlobe = false;

    public static void main(String[] args) {
        try {
            if (args[0].equals("globe")) {
                doGlobe = true;
            }
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }
        launch(args);
    }

    //Tracks drag starting point for x and y
    private double anchorX, anchorY;
    //Keep track of current angle for x and y
    private double anchorAngleX = 0;
    private double anchorAngleY = 0;
    private boolean mouseClicked = false;

    @Override
    public void start(Stage primaryStage) {

        //Scene map = new Scene(PlanetTerrainGenerator.generateTerrain(800, 400, 0), 800, 400, Color.WHITE);

        if (doGlobe) {

            primaryStage.setResizable(false);

            Camera cam = new PerspectiveCamera(true);

            Scene kugel = new Scene(PlateGenerator.buildGlobe(0, 1, 100), 500, 500, true);
            kugel.getRoot().setTranslateZ(10);
            kugel.setCamera(cam);

            Rotate xRotate, yRotate;
            kugel.getRoot().getTransforms().addAll(
                    xRotate = new Rotate(0, Rotate.X_AXIS),
                    yRotate = new Rotate(0, Rotate.Y_AXIS)
            );

            Timeline update = new Timeline(new KeyFrame(Duration.millis(100), event -> {
                yRotate.setAngle(yRotate.getAngle() + 2);
            }));
            update.setCycleCount(Timeline.INDEFINITE);
            update.play();

            kugel.setOnMousePressed(event -> {
                anchorX = event.getSceneX();
                anchorY = event.getSceneY();
                anchorAngleX = xRotate.getAngle();
                anchorAngleY = yRotate.getAngle();
                mouseClicked = true;
            });

            kugel.setOnMouseReleased(event -> mouseClicked = false);

            kugel.setOnMouseMoved(event -> {
                xRotate.setAngle(anchorAngleX - (anchorY - event.getSceneY()));
                yRotate.setAngle(anchorAngleY + anchorX - event.getSceneX());
            });

            kugel.setOnKeyPressed(event -> {
                switch (event.getCode()) {
                    case W:
                        kugel.getCamera().setTranslateY(kugel.getCamera().getTranslateY() - 0.5);
                        break;
                    case S:
                        kugel.getCamera().setTranslateY(kugel.getCamera().getTranslateY() + 0.5);
                        break;
                    case A:
                        kugel.getCamera().setTranslateX(kugel.getCamera().getTranslateX() - 0.5);
                        break;
                    case D:
                        kugel.getCamera().setTranslateX(kugel.getCamera().getTranslateX() + 0.5);
                        break;

                }
            });

            kugel.setOnMouseDragged(event -> {
                //event.get
            });

            primaryStage.setScene(kugel);
        } else {
	        Scene map = new Scene(PlateGenerator.buildStereographicMap(0, 1, 20), 500, 500, true);
	        //map.getRoot().setTranslateZ(10);

            Translate centering = new Translate();
            centering.xProperty().bind(primaryStage.widthProperty().divide(2));
            centering.yProperty().bind(primaryStage.heightProperty().divide(2));


            Scale scaling = new Scale();
            scaling.xProperty().bind(scaling.yProperty());
            scaling.yProperty().bind(primaryStage.widthProperty().divide(16));
            scaling.pivotXProperty().bind(centering.xProperty());
            scaling.pivotYProperty().bind(centering.yProperty());

            //Transform mix = centering.createConcatenation(scaling);

            map.getRoot().getTransforms().addAll(scaling, centering);
            //map.getRoot().scaleXProperty().bind(map.getRoot().scaleYProperty());
            //map.getRoot().scaleYProperty().bind(primaryStage.widthProperty().divide(16));

            //map.getRoot().scaleYProperty().bind(map.getRoot().scaleXProperty());
            //map.getRoot().scaleXProperty().bind(primaryStage.widthProperty().divide(50));
            //map.setCamera(new PerspectiveCamera(true));

            //map.getRoot().translateXProperty().bind(primaryStage.widthProperty().divide(2));
            //map.getRoot().translateYProperty().bind(primaryStage.heightProperty().divide(2));

            primaryStage.setScene(map);
            primaryStage.show();
            System.out.println(map.getRoot().getTransforms());
        }
    }
}
