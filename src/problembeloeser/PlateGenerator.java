package problembeloeser;

import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.*;
import quickhull3d.Point3d;
import quickhull3d.QuickHull3D;

import javax.imageio.ImageIO;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class PlateGenerator {

    private static QuickHull3D buildQuickHull(ArrayList<Point3d> centers) {
        QuickHull3D hull = new QuickHull3D();
        hull.build(centers.toArray(new Point3d[0]));
        return hull;
    }

    private static ArrayList<Polygon> buildVoronoi(ArrayList<Point2D> centers) {
        ArrayList<Point3d> projectedCenters = new ArrayList<>();
        for (Point2D center : centers) {
            projectedCenters.add(new Point3d(center.getX(), center.getY(), Math.pow(center.getX(), 2) + Math.pow(center.getY(), 2)));
        }

        QuickHull3D hull = new QuickHull3D();
        hull.build(projectedCenters.toArray(new Point3d[0]));
        int[][] faces = hull.getFaces();

        ArrayList<Polygon> result = new ArrayList<>();
        for (int i = 0; i < faces.length; i++) {
            Polygon poly = new Polygon();
            for (int index : faces[i]) {
                poly.getPoints().addAll(projectedCenters.get(index).x, projectedCenters.get(index).y);
            }
            result.add(poly);
        }
        return result;
    }

    private static Point2D getRandomSpherePolar(Random rand) {
        double alpha = (rand.nextDouble() * 2 - 1) * Math.PI;
        double theta = (rand.nextDouble() - 0.5) * Math.PI;
        return new Point2D(alpha, theta);
    }

    private static Point2D projectEquirectangular(Point2D input, int radius) {
        double width = 2 * Math.PI * radius;
        double x = width * (input.getX() + Math.PI) / Math.PI;
        double y = width * (input.getY() + Math.PI) / Math.PI;
        return new Point2D(x, y);
    }

    private static Point2D stereoGraphicToEquirectangular(Point2D input, int radius) {
        return projectEquirectangular(stereographicToSphere(input, radius), radius);
    }

    private static Point2D stereographicToSphere(Point2D input, int radius) {
        double alpha = Math.atan2(input.getY(), input.getX());
        double theta = 2 * Math.atan(Math.sqrt(Math.pow(input.getX(), 2) + Math.pow(input.getY(), 2) / (2 * radius)));
        return new Point2D(alpha, theta);
    }

    private static Point2D projectStereographic(Point2D input, int radius) {
        double m = 2 * radius * Math.tan((input.getY() + Math.PI / 2.0) / 2.0);
        return new Point2D(m * Math.cos(input.getX()), m * Math.sin(input.getX()));
    }

    private static double getOrthodromeDist(Point2D point1, Point2D point2) {
        return Math.acos(Math.sin(point1.getY()) * Math.sin(point2.getY()) + Math.cos(point1.getY()) * Math.cos(point2.getY()) * Math.cos(point1.getX() - point2.getX()));
    }

    private static int getIndexOfNearest(ArrayList<Point2D> centers, Point2D point) {
        double[] dists = new double[centers.size()];
        for (int i = 0; i < centers.size(); i++) {
            dists[i] = getOrthodromeDist(centers.get(i), point);
        }
        int minIndex = -1;
        double minValue = Double.MAX_VALUE;
        for (int i = 0; i < centers.size(); i++) {
            if (dists[i] < minValue) {
                minValue = dists[i];
                minIndex = i;
            }
        }
        return minIndex;
    }

    private static Image generateSnapshot(Group root) throws IOException {
        WritableImage img = root.snapshot(new SnapshotParameters(), null);

        File output = File.createTempFile("snapshot", ".png");

        ImageIO.write(SwingFXUtils.fromFXImage(img, null), "png", output);

        Image result = new Image(output.getAbsolutePath());

        return result;
    }

    public static Group buildEquirectangularMap(ArrayList<Point2D> sphericalCenters, int radius) {
        int width = (int) (2 * radius * Math.PI);

        ArrayList<Point2D> equirectangularCenters = new ArrayList<>();
        for (Point2D sphericalCenter : sphericalCenters) {
            equirectangularCenters.add(projectEquirectangular(sphericalCenter, width));
        }


        Group result = new Group();
        Sphere globe = new Sphere(1, 20);
        result.getChildren().add(globe);
        PhongMaterial mat = new PhongMaterial();
        //mat.setDiffuseMap(generateSnapshot());
        globe.setMaterial(mat);
        return result;
    }

    private static boolean isInside(Polygon shape, Point2D point) {
        Line ray = new Line(-1, -1, point.getX(), point.getY());
        ArrayList<Line> edges = new ArrayList<>();
        for (int i = 0; i < shape.getPoints().size(); i += 2) {
            edges.add(new Line(shape.getPoints().get((i - 2) % shape.getPoints().size()), shape.getPoints().get((i - 1) % shape.getPoints().size()), shape.getPoints().get(i), shape.getPoints().get(i + 1)));
        }
        Path[] intersections = new Path[edges.size()];
        for (int i = 0; i < intersections.length; i++) {
            intersections[i] = (Path) Shape.intersect(ray, edges.get(i));
        }
        int count = 0;
        for (Path intersection : intersections) {
            for (PathElement e : intersection.getElements()) {
                if (e.getClass() == ClosePath.class) {
                    count++;
                }
            }
        }
        return count % 2 == 1;
    }

    public static Group buildStereographicMap(int radius, int amount) {
        Random rand = new Random();
        long seed = rand.nextLong();
        System.out.println("Seed: " + seed);
        return buildStereographicMap(seed, radius, amount);
    }

    private static String calculateNormalVector(Point3d a, Point3d b, Point3d c) {
        Point3d vec1 = new Point3d(a.x - b.x, a.y - b.y, a.z - b.z);
        Point3d vec2 = new Point3d(a.x - c.x, a.y - c.y, a.z - c.z);
        double x = vec1.y * vec2.z - vec1.z * vec2.y;
        double y = vec1.z * vec2.x - vec1.x * vec2.z;
        double z = vec1.x * vec2.y - vec1.y * vec2.x;
        double len = Math.sqrt(x * x + y * y + z * z);
        x /= len;
        y /= len;
        z /= len;
        return x + " " + y + " " + z;
    }

    private static String pointToLine(Point3d input) {
        return "vertex " + input.x + " " + input.y + " " + input.z + "\n";
    }

    private static void writeSTLFile(QuickHull3D hull) {
        StringBuilder content = new StringBuilder("solid mesh\n");
        Point3d[] vertices = hull.getVertices();
        for (int[] face : hull.getFaces()) {
            content.append("facet normal ").append(calculateNormalVector(vertices[face[0]], vertices[face[1]], vertices[face[2]])).append("\n");
            content.append("outer loop\n");
            for (int index : face) {
                content.append(pointToLine(vertices[index]));
            }
            content.append("endloop\nendfacet\n");
        }
        content.append("endsolid mesh\n");
        File output = new File("hullMesh.stl");
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(output));
            bw.write(content.toString(), 0, content.length());
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Point2D cartesianToPolar(Point3d input) {
        double radius = Math.sqrt(input.x * input.x + input.y * input.y + input.z * input.z);
        double theta = Math.asin(input.z / radius);
        double alpha = Math.atan2(input.y, input.x);
        return new Point2D(alpha, theta);
    }

    public static Group buildStereographicMap(long seed, int radius, int amount) {
        Random rand = new Random(seed);
        Group map = new Group();

        ArrayList<Point2D> sphericalCenters = new ArrayList<>();
        ArrayList<Point2D> stereographicCenters = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            Point2D center = getRandomSpherePolar(rand);
            sphericalCenters.add(center);
            stereographicCenters.add(projectStereographic(center, radius));
        }
        ArrayList<Point3d> cartesianCenters = new ArrayList<>();
        for (Point2D center : sphericalCenters) {
            double x = Math.cos(center.getX()) * Math.cos(center.getY());
            double y = Math.sin(center.getX()) * Math.cos(center.getY());
            double z = Math.sin(center.getY());
            cartesianCenters.add(new Point3d(x, y, z));
        }

        QuickHull3D hull = new QuickHull3D();
        hull.build(cartesianCenters.toArray(new Point3d[0]));
        for (Point3d vertex : hull.getVertices()) {
            Sphere kuller = new Sphere(0.05, 12);
            kuller.setTranslateX(vertex.x);
            kuller.setTranslateY(vertex.y);
            kuller.setTranslateZ(vertex.z);
            PhongMaterial mat = new PhongMaterial();
            mat.setDiffuseColor(Color.RED);
            kuller.setMaterial(mat);
        }
        writeSTLFile(hull);

        ArrayList<Point2D> hullVertices = new ArrayList<>();
        Group hullVertexGroup = new Group();
        map.getChildren().add(hullVertexGroup);
        for (Point3d vertex : hull.getVertices()) {
            hullVertices.add(projectStereographic(cartesianToPolar(vertex), radius));
        }

        Group stereoCenterGroup = new Group();
        map.getChildren().add(stereoCenterGroup);
        for (Point2D stereographicalCenter : stereographicCenters) {
            stereoCenterGroup.getChildren().add(new Circle(stereographicalCenter.getX(), stereographicalCenter.getY(), 0.05, Color.BLUE));
        }
        map.getChildren().add(new Circle(0, 0, 0.05, Color.BLACK));
        Group edges = new Group();
        map.getChildren().add(edges);
        for (int[] faces : hull.getFaces()) {
            Line edge1 = new Line(hullVertices.get(faces[0]).getX(), hullVertices.get(faces[0]).getY(), hullVertices.get(faces[1]).getX(), hullVertices.get(faces[1]).getY());
            edge1.setStrokeWidth(0.01);
            edge1.setStroke(Color.PURPLE);
            Line edge2 = new Line(hullVertices.get(faces[1]).getX(), hullVertices.get(faces[1]).getY(), hullVertices.get(faces[2]).getX(), hullVertices.get(faces[2]).getY());
            edge2.setStrokeWidth(0.01);
            edge2.setStroke(Color.PURPLE);
            Line edge3 = new Line(hullVertices.get(faces[2]).getX(), hullVertices.get(faces[2]).getY(), hullVertices.get(faces[0]).getX(), hullVertices.get(faces[0]).getY());
            edge3.setStrokeWidth(0.01);
            edge3.setStroke(Color.PURPLE);
            edges.getChildren().addAll(edge1, edge2, edge3);
        }

        Circle unitCircle = new Circle();
        unitCircle.setCenterX(0);
        unitCircle.setCenterY(0);
        unitCircle.setRadius(1);
        unitCircle.setFill(Color.TRANSPARENT);
        unitCircle.setStroke(Color.BLACK);
        unitCircle.setStrokeWidth(0.1);
        map.getChildren().add(unitCircle);

        double maxDist = 0;
        for (Point2D stereoCenter : stereographicCenters) {
            maxDist = Math.max(maxDist, Math.sqrt(Math.pow(stereoCenter.getX(), 2) + Math.pow(stereoCenter.getY(), 2)));
        }

        float[][] points = new float[sphericalCenters.size()][2];
        for (int i = 0; i < points.length; i++) {

        }

        //TODO Voronoi-Diagramm

        return map;
    }

    public static Group buildGlobe(long seed, int radius, int amount) {
        Random rand = new Random(seed);
        Group result = new Group();

        ArrayList<Point2D> centers = new ArrayList<>();
        ArrayList<Color> colors = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            centers.add(getRandomSpherePolar(rand));
            colors.add(Color.color(rand.nextDouble(), rand.nextDouble(), rand.nextDouble()));
        }
        int resolution = 1;
        Sphere[][] voxels = new Sphere[360 / resolution][360 / resolution];
        for (int alpha = 0; alpha < 360 / resolution; alpha++) {
            for (int theta = 0; theta < 180 / resolution; theta++) {
                voxels[alpha][theta] = new Sphere(0.08 / 5 * resolution, 10);
                voxels[alpha][theta].setTranslateX(radius * Math.cos(Math.toRadians(alpha * resolution)) * Math.cos(Math.toRadians((theta - 90) * resolution)));
                voxels[alpha][theta].setTranslateY(radius * Math.sin(Math.toRadians(alpha * resolution)) * Math.cos(Math.toRadians((theta - 90) * resolution)));
                voxels[alpha][theta].setTranslateZ(radius * Math.sin(Math.toRadians((theta - 90) * resolution)));
                PhongMaterial mat = new PhongMaterial();
                mat.setDiffuseColor(colors.get(getIndexOfNearest(centers, new Point2D(Math.toRadians(alpha * resolution), Math.toRadians((theta - 90) * resolution)))));
                //mat.setDiffuseColor(Color.RED);
                voxels[alpha][theta].setMaterial(mat);
                result.getChildren().add(voxels[alpha][theta]);
            }
        }
        return result;
    }

}
