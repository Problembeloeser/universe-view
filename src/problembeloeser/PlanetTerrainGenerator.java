package problembeloeser;

import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.text.Text;
import quickhull3d.Point3d;
import quickhull3d.QuickHull3D;

import java.util.*;

public class PlanetTerrainGenerator {

    private static void generateVoronoi(ArrayList<Point2D> centers, int width, int height){
        ArrayList<Point3d> projecteds = new ArrayList<>();
        for (Point2D center : centers){
            projecteds.add(new Point3d(center.getX(), center.getY(), Math.pow(center.getX(), 2) + Math.pow(center.getY(), 1)));
        }
        QuickHull3D hull = new QuickHull3D();
        hull.build(projecteds.toArray(new Point3d[0]));
        int[][] faces = hull.getFaces();
        for (int i = 0; i < faces.length; i++){

        }
    }

    private static Color randomColor(Random rand){
        return Color.color(rand.nextDouble(), rand.nextDouble(), rand.nextDouble());
    }

    private static ArrayList<Point2D> shapeToPoints(Path shape) {
        ArrayList<Point2D> result = new ArrayList<>();
        for (PathElement e : shape.getElements()) {
            if (e.getClass() == MoveTo.class) {
                result.add(new Point2D(((MoveTo) e).getX(), ((MoveTo) e).getY()));
            } else if (e.getClass() == LineTo.class) {
                result.add(new Point2D(((LineTo) e).getX(), ((LineTo) e).getY()));
            }
        }
        return result;
    }

    private static Polygon shapeToPolygon(Path shape) {
        ArrayList<Point2D> points = shapeToPoints(shape);
        Polygon result = new Polygon();
        for (Point2D point : points) {
            result.getPoints().addAll(point.getX(), point.getY());
        }
        return result;
    }

    private static ArrayList<Point2D> polygonToPoints(Polygon poly) {
        ArrayList<Point2D> result = new ArrayList<>();
        for (int i = 0; i < poly.getPoints().size(); i += 2) {
            result.add(new Point2D(poly.getPoints().get(i), poly.getPoints().get(i + 1)));
        }
        return result;
    }

    private static ArrayList<Path> triangulateShape(Path input) {
        Path shape = new Path();
        shape.getElements().addAll(input.getElements());
        ArrayList<Path> result = new ArrayList<>();
        while (shape.getElements().size() >= 4){
            int leftIndex = -1;
            double leftMost = Double.MAX_VALUE;
            ArrayList<Point2D> points = shapeToPoints(shape);
            for (int i = 0; i < points.size(); i++) {
                if (points.get(i).getX() < leftMost) {
                    leftIndex = i;
                }
            }
            int leftNeighbor = (leftIndex - 1) % points.size(), rightNeighbor = (leftIndex + 1) % points.size();

            Polygon triangle = new Polygon();
            triangle.getPoints().addAll(points.get(leftIndex).getX(), points.get(leftIndex).getY());
            triangle.getPoints().addAll(points.get(leftNeighbor).getX(), points.get(leftNeighbor).getY());
            triangle.getPoints().addAll(points.get(rightNeighbor).getX(), points.get(rightNeighbor).getY());

            result.add((Path) Shape.union(triangle, new Path()));
            System.out.println("Separate Triangle");
            shape = (Path) Shape.subtract(shape, triangle);
        }
        result.add(shape);
        return result;
    }

    private static double approximateArea(Path shap, int width, int height) {
        Point2D leftBorder = getAngles(new Point2D(shap.getLayoutBounds().getMinX(), shap.getLayoutBounds().getMinY()), width, height);
        Point2D rightBorder = getAngles(new Point2D(shap.getLayoutBounds().getMaxX(), shap.getLayoutBounds().getMaxY()), width, height);
        double area = 1;
        area *= Math.abs(leftBorder.getX() - rightBorder.getX()) / (Math.PI * 2);
        area *= Math.abs(leftBorder.getY() - rightBorder.getY()) / Math.PI;
        return area * 0.4;
    }

    public static Point2D getRandomVelocityVector(Random rand) {
        double ratio = rand.nextDouble();
        double velocity = rand.nextGaussian() * 2 + 5;
        velocity = rand.nextBoolean() ? velocity : -velocity;
        return new Point2D(Math.atan(ratio) * velocity, Math.atan(1.0 / ratio) * velocity);
    }

    public static Group generateTerrain(int width, int height) {
        Random rand = new Random();
        long seed = rand.nextLong();
        System.out.println("Seed: " + seed);
        return generateTerrain(width, height, seed);
    }

    private static Point2D getAngles(Point2D point, int width, int height) {
        Point2D result = new Point2D((point.getX() - width / 2.0) / width * 2 * Math.PI, (point.getY() - height / 2.0) / height * Math.PI);
        return result;
    }

    private static double getSphericalDist(Point2D point1, Point2D point2, double radius) {
        double a = Math.acos(Math.sin(point1.getY()) * Math.sin(point2.getY()) + Math.cos(point1.getY()) * Math.cos(point2.getY()) * Math.cos(point2.getX() - point1.getX()));
        return radius * a;
    }

    private static int getNearestPoint(ArrayList<Point2D> centers, int width, int height, Point2D input) {
        Point2D point = getAngles(input, width, height);
        int min = 0;
        double minValue = Double.MAX_VALUE;
        for (int i = 0; i < centers.size(); i++) {
            if (minValue > getSphericalDist(getAngles(centers.get(i), width, height), point, width / (2 * Math.PI))) {
                min = i;
                minValue = getSphericalDist(getAngles(centers.get(i), width, height), point, width / (2 * Math.PI));
            }
        }
        return min;
    }

    private static int getRandomWeightedIndex(ArrayList<Double> probabilites, Random rand) {
        double result = rand.nextDouble();
        for (int i = 0; i < probabilites.size(); i++) {
            result -= probabilites.get(i);
            if (result <= 0) {
                return i;
            }
        }
        return -1;
    }

    private static double getDist(int x1, int y1, int x2, int y2) {
        return getDist(new Point2D(x1, y1), new Point2D(x2, y2));
    }

    private static double getDist(Point2D point1, Point2D point2) {
        return Math.sqrt(Math.pow(point1.getX() - point2.getX(), 2) + Math.pow(point1.getY() - point2.getY(), 2));
    }

    private static double getPathLength(Path path) {
        double result = 0;
        int x = (int) ((MoveTo) path.getElements().get(0)).getX();
        int y = (int) ((MoveTo) path.getElements().get(0)).getY();
        for (int i = 1; i < path.getElements().size(); i++) {
            try {
                result += getDist(x, y, (int) ((LineTo) path.getElements().get(i)).getX(), (int) ((LineTo) path.getElements().get(i)).getY());
                x = (int) ((LineTo) path.getElements().get(i)).getX();
                y = (int) ((LineTo) path.getElements().get(i)).getY();
            } catch (ClassCastException ignored) {
            }
        }
        return result;
    }

    private static ArrayList<Double> generateProbabilityTable(ArrayList<Point2D> centers, ArrayList<Path> areas, int center) {
        double summedDistance = 0;
        double maxDist = 0;
        ArrayList<Double> result = new ArrayList<>();
        for (int i = 0; i < centers.size(); i++) {
            if (i == center) {
                result.add(Double.MAX_VALUE);
                continue;
            }
            if (areAdjacent(areas.get(center), areas.get(i))) {
                // Maybe change the influence of the shared border. Current: Square Root
                result.add(getDist(centers.get(i), centers.get(center)) - Math.sqrt(getPathLength((Path) Shape.intersect(areas.get(center), areas.get(i)))));
                maxDist = Math.max(result.get(result.size() - 1), maxDist);
                summedDistance += result.get(result.size() - 1);
            } else {
                result.add(Double.MAX_VALUE);
            }
        }
        for (int i = 0; i < result.size(); i++) {
            if (i != center) {
                result.set(i, Math.max(maxDist - result.get(i), 0) / summedDistance);
            }
        }
        return result;
    }

    private static boolean isInside(Shape bounds, Shape object) {
        return ((Path) Shape.union(bounds, object)).getElements().size() == ((Path) Shape.union(bounds, bounds)).getElements().size();
    }

    private static boolean areAdjacent(Path shape1, Path shape2) {
        return shape1.getElements().size() + shape2.getElements().size() - ((Path) Shape.union(shape1, shape2)).getElements().size() > 0;
    }

    private static Shape mergeList(ArrayList<Rectangle> list, int begin, int end) {
        if (begin >= end) {
            try {
                return list.get(begin);
            } catch (IndexOutOfBoundsException e) {
                return new Path();
            }
        } else {
            Shape result = Shape.union(mergeList(list, begin, (begin + end) / 2), mergeList(list, (begin + end) / 2 + 1, end));
            return result;
        }
    }

    private static ArrayList<Point2D> buildCenters(Shape bounds, Random rand, int initialNumber) {
        System.out.println("Started finding resonable center spots inside bounds");
        ArrayList<Point2D> result = new ArrayList<>();
        Bounds boundingBox = bounds.getLayoutBounds();
        for (int i = 0; i < initialNumber; i++) {
            int x, y;
            do {
                x = rand.nextInt((int) (boundingBox.getMaxX() - boundingBox.getMinX())) + (int) boundingBox.getMinX();
                y = rand.nextInt((int) (boundingBox.getMaxY() - boundingBox.getMinY())) + (int) boundingBox.getMinY();
            } while (!isInside(bounds, new Circle(x, y, 10)));
            result.add(new Point2D(x, y));
        }
        return result;
    }

    private static int[][] buildNearest(ArrayList<Point2D> centers, int width, int height) {
        System.out.println("Started computing pixel distances");
        int[][] result = new int[width][height];
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                result[x][y] = getNearestPoint(centers, width, height, new Point2D(x, y));
            }
        }
        return result;
    }

    private static HashMap<Integer, ArrayList<Rectangle>> buildRegions(int[][] nearest, int width, int height, int amount) {
        System.out.println("Started mapping pixels");
        HashMap<Integer, ArrayList<Rectangle>> result = new HashMap<>();
        for (int i = 0; i < amount; i++) {
            result.put(i, new ArrayList<>());
        }
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                result.get(nearest[x][y]).add(new Rectangle(x, y, 1, 1));
            }
        }
        return result;
    }

    private static int getSubdivisions(Path shape) {
        int result = 0;
        for (PathElement e : shape.getElements()) {
            if (e.getClass() == ClosePath.class) {
                result++;
            }
        }
        return result;
    }

    private static ArrayList<Path> splitShape(Path shape) {
        ArrayList<Path> result = new ArrayList<>();
        for (int i = 0; i < getSubdivisions(shape); i++) {
            result.add(new Path());
        }
        Iterator<PathElement> it = shape.getElements().iterator();
        for (int current = 0; current < result.size(); current++) {
            PathElement e;
            do {
                e = it.next();
                result.get(current).getElements().add(e);
            } while (e.getClass() != ClosePath.class);
        }
        return result;
    }

    private static boolean isConnected(Path shape, int width) {
        // Only works for 2 subdivisions
        int count = getSubdivisions(shape);
        if (count == 1) {
            return true;
        } else if (count == 2) {
            ArrayList<Point2D>[] borders = new ArrayList[count];
            for (int i = 0; i < count; i++) {
                borders[i] = new ArrayList<>();
            }
            Iterator<PathElement> it = shape.getElements().iterator();
            for (int part = 0; part < count; part++) {
                PathElement e = it.next();
                while (e.getClass() != ClosePath.class) {
                    if (e.getClass() == MoveTo.class) {
                        borders[part].add(new Point2D(((MoveTo) e).getX(), ((MoveTo) e).getY()));
                    } else if (e.getClass() == LineTo.class) {
                        borders[part].add(new Point2D(((LineTo) e).getX(), ((LineTo) e).getY()));

                    }
                    e = it.next();
                }
            }
            ArrayList<Double>[][] wrapBorders = new ArrayList[count][2];
            for (int i = 0; i < count; i++) {
                for (int j = 0; j < 2; j++) {
                    wrapBorders[i][j] = new ArrayList<>();
                }
            }
            for (int i = 0; i < count; i++) {
                for (Point2D point : borders[i]) {
                    if (point.getX() == 0) {
                        wrapBorders[i][0].add(point.getY());
                    } else if (point.getX() == width) {
                        wrapBorders[i][1].add(point.getY());
                    }
                }
            }
            for (Double y : wrapBorders[0][0]) {
                if (wrapBorders[1][1].contains(y)) {
                    return true;
                }
            }
            for (Double y : wrapBorders[0][1]) {
                if (wrapBorders[1][0].contains(y)) {
                    return true;
                }
            }
            return false;
        } else {
            throw new IllegalArgumentException("More than 2 subdivisions");
        }
    }

    private static void mergeNearest(int[][] nearest, int mergeParent, int mergeChild, int width, int height) {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                switch ((int) Math.signum(nearest[x][y] - mergeChild)) {
                    case 0:
                        nearest[x][y] = mergeParent;
                        break;
                    case 1:
                        nearest[x][y]--;
                        break;
                }
            }
        }
    }

    private static double getPathArea(Path shape, double radius) {
        shape.setStrokeType(StrokeType.INSIDE);
        int area = 0;
        for (double x = shape.getLayoutBounds().getMinX(); x < shape.getLayoutBounds().getMaxX(); x++) {
            for (double y = shape.getLayoutBounds().getMinY(); y < shape.getLayoutBounds().getMaxY(); y++) {
                if (isInside(shape, new Rectangle(x, y, 1, 1))) {
                    area++;
                }
            }
        }
        shape.setStrokeType(StrokeType.CENTERED);
        return area;
    }

    private static ArrayList<Path> buildSubVoronoi(ArrayList<Rectangle> region, ArrayList<Point2D> subCenters, Random rand, int width, int height) {
        ArrayList<Double> scaleFactors = new ArrayList<>();
        for (int i = 0; i < subCenters.size(); i++) {
            scaleFactors.add(1.0);
        }
        ArrayList[] subRegions = new ArrayList[subCenters.size()];
        for (int j = 0; j < subRegions.length; j++) {
            subRegions[j] = new ArrayList<>();
        }
        for (Rectangle rect : region) {
            subRegions[getNearestPoint(subCenters, width, height, new Point2D(rect.getX(), rect.getY()))].add(rect);
        }
        ArrayList<Path> subAreas = new ArrayList<>();
        for (int j = 0; j < subCenters.size(); j++) {
            System.out.println("Started merging subregion " + j);
            subAreas.add((Path) mergeList(subRegions[j], 0, subRegions[j].size() - 1));
            subAreas.get(j).setFill(Color.color(rand.nextDouble(), rand.nextDouble(), rand.nextDouble()));
            subAreas.get(j).setStroke(Color.BLACK);
            subAreas.get(j).setStrokeWidth(2);
            subAreas.get(j).setStrokeType(StrokeType.CENTERED);
        }
        return subAreas;
    }

    private static ArrayList<Path> buildVoronoi(int[][] nearest, ArrayList<Point2D> centers, Random rand, int width, int height) {
        HashMap<Integer, ArrayList<Rectangle>> regions = buildRegions(nearest, width, height, centers.size());

        ArrayList<Path> areas = new ArrayList<>();
        for (int i = 0; i < centers.size(); i++) {
            System.out.println("Started merging region " + i);
            areas.add((Path) mergeList(regions.get(i), 0, Math.max(regions.get(i).size() - 1, 0)));
            areas.get(i).setFill(Color.color(rand.nextDouble(), rand.nextDouble(), rand.nextDouble()));
            areas.get(i).setStroke(Color.BLACK);
            areas.get(i).setStrokeWidth(2);
            areas.get(i).setStrokeType(StrokeType.CENTERED);
        }
        return areas;
    }

    public static Group generateTerrain(int width, int height, long seed) {
        Random rand = new Random(seed);
        Group result = new Group();

        int initialNumber = 8;
        try {
            ArrayList<Point2D> centers = buildCenters(new Rectangle(0, 0, width, height), rand, initialNumber);
            generateVoronoi(centers, width, height);

            ArrayList<Path> areas;

            int[][] nearest = buildNearest(centers, width, height);
            areas = buildVoronoi(nearest, centers, rand, width, height);

            ArrayList<Circle> centerMarkers = new ArrayList<>();
            ArrayList<Text> labels = new ArrayList<>();

            // Merging-Time

            int[] pairs = new int[2];
            int merges = initialNumber / 3;
            int[] mergedPlates = new int[merges];
            for (int i = 0; i < merges; i++) {
                do {
                    pairs[0] = rand.nextInt(centers.size());
                    pairs[1] = getRandomWeightedIndex(generateProbabilityTable(centers, areas, pairs[0]), rand);
                } while (!areAdjacent(areas.get(pairs[0]), areas.get(pairs[1])) || pairs[0] == pairs[1]);
                if (pairs[1] < pairs[0]) {
                    pairs[0] = pairs[0] ^ pairs[1];
                    pairs[1] = pairs[0] ^ pairs[1];
                    pairs[0] = pairs[0] ^ pairs[1];
                }
                mergedPlates[i] = pairs[0];
                mergeNearest(nearest, pairs[0], pairs[1], width, height);
                areas.set(pairs[0], (Path) Shape.union(areas.get(pairs[0]), areas.get(pairs[1])));
                areas.remove(pairs[1]);
                for (int j = 0; j < merges; j++) {
                    if (pairs[1] < mergedPlates[j]) {
                        mergedPlates[j]--;
                    }
                }
                System.out.println("Merged " + pairs[0] + " with " + pairs[1]);

                centers.remove(pairs[1]);
            }
            Arrays.sort(mergedPlates);


            // Splitting-Time
            for (int i = mergedPlates.length - 1; i >= 0; i--) {
                int subPlates = 0;
                for (int plate : mergedPlates) {
                    if (plate == mergedPlates[i]) {
                        subPlates += 3;
                    }
                }
                ArrayList<Point2D> subCenters = buildCenters(areas.get(mergedPlates[i]), rand, subPlates);
                areas.addAll(buildSubVoronoi(buildRegions(nearest, width, height, areas.size()).get(mergedPlates[i]), subCenters, rand, width, height));
                centers.addAll(subCenters);
                areas.remove(mergedPlates[i]);
                centers.remove(mergedPlates[i]);
                i -= (subPlates / 3 - 1);
            }

            ArrayList<Path> newAreas = new ArrayList<>();
            Iterator<Path> it = areas.iterator();
            while (it.hasNext()) {
                Path shape = it.next();
                if (getSubdivisions(shape) > 1) {
                    if (!isConnected(shape, width)) {
                        centers.remove(areas.indexOf(shape));
                        for (Path part : splitShape(shape)) {
                            if (part.getLayoutBounds().getWidth() * part.getLayoutBounds().getHeight() > 25) {
                                newAreas.add(part);
                                centers.add(new Point2D((part.getLayoutBounds().getMinX() + part.getLayoutBounds().getMaxX()) / 2.0, (part.getLayoutBounds().getMinY() + part.getLayoutBounds().getMaxY()) / 2.0));
                            }
                        }
                        it.remove();
                    }
                }
            }

            areas.addAll(newAreas);
            nearest = buildNearest(centers, width, height);

            for (Point2D center : centers) {
                centerMarkers.add(new Circle(center.getX(), center.getY(), 5, Color.BLACK));
                Text text = new Text();
                text.setX(center.getX());
                text.setY(center.getY());
                text.setFill(Color.WHITE);
                text.setText(centers.indexOf(center) + "");
                labels.add(text);
            }

            for (Path area : areas) {
                area.setFill(randomColor(rand));
                area.setStroke(Color.TRANSPARENT);
                area.setStrokeWidth(2);
                area.setStrokeType(StrokeType.CENTERED);
            }

            ArrayList<Path> triangles = triangulateShape(areas.get(0));
            for (Path triangle : triangles) {
                triangle.setFill(Color.color(rand.nextDouble(), rand.nextDouble(), rand.nextDouble()));
            }

            result.getChildren().addAll(triangles);
            areas.get(0).setFill(Color.BLACK);
            //result.getChildren().add(areas.get(0));

            //result.getChildren().addAll(areas);
            //result.getChildren().addAll(centerMarkers);
            //result.getChildren().addAll(labels);

            return result;
        } catch (IllegalArgumentException e) {
            return generateTerrain(width, height);
        }
    }
}
